/*
 * Copyright (c) 2005 Your Corporation. All Rights Reserved.
 */
package com.atlassian.jira.plugin.linker;

/**
 * Created by IntelliJ IDEA.
 * User: david
 * Date: Nov 18, 2005
 * Time: 11:18:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class SearchException extends Exception
{
    public SearchException()
    {
    }

    public SearchException(String message)
    {
        super(message);
    }

    public SearchException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public SearchException(Throwable cause)
    {
        super(cause);
    }
}
