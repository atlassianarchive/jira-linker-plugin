/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

package com.atlassian.jira.plugin.linker;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsManager;
import com.opensymphony.util.TextUtils;
import org.apache.log4j.Logger;
import webwork.action.ActionContext;

public class ConfluencePageBrowserAction extends JiraWebActionSupport
{
    private Logger log = Logger.getLogger(ConfluencePageBrowserAction.class);

    private SearchResultsBean results;
    private SearchQueryBean query;

    private String doSearch;
    private static final String SEARCH_RESULTS_KEY = "com.atlassian.jira.plugin.linker:results";
    private static final String SEARCH_QUERY_KEY = "com.atlassian.jira.plugin.linker:query";
    private int startIndex = -1;
    private String doReset;

    private TrustedApplicationsManager trustedApplicationsManager;

    public ConfluencePageBrowserAction(TrustedApplicationsManager trustedApplicationsManager)
    {
        this.trustedApplicationsManager = trustedApplicationsManager;
    }

    public String doDefault()
    {
        return null;
    }

    protected String doExecute() throws Exception
    {
        if (doSearch != null)
        {
            log.debug("doSearch = '" + doSearch + "'");
            resetResults();
            getResults().setItems(getQuery().doSearch());
        }
        else if (doReset != null)
        {
            resetResults();
        }
        else if (startIndex > -1)
        {
            log.debug("startIndex = " + startIndex);
            getResults().setStartIndex(startIndex);
        }

        return super.doExecute();
    }

    private void resetResults()
    {
        log.debug("resetting results");
        results = null;
        ActionContext.getSession().remove(SEARCH_RESULTS_KEY);
    }

    public SearchQueryBean getQuery()
    {
        if (query == null)
        {
            log.debug("query is null");
            query = (SearchQueryBean)ActionContext.getSession().get(SEARCH_QUERY_KEY);
            if (query == null)
            {
                log.debug("query was not in session");
                query = new SearchQueryBean(getRemoteUser(), trustedApplicationsManager);
                ActionContext.getSession().put(SEARCH_QUERY_KEY, query);
                log.debug("query has been created and put into session");
            }
        }
        return query;
    }

    public boolean hasResults()
    {
        return getResults() != null && getResults().getTotal() > 0;
    }

    public SearchResultsBean getResults()
    {
        if (results == null)
        {
            log.debug("results was null");
            results = (SearchResultsBean)ActionContext.getSession().get(SEARCH_RESULTS_KEY);
            if (results == null)
            {
                log.debug("results was not in session");
                results = new SearchResultsBean();
                ActionContext.getSession().put(SEARCH_RESULTS_KEY, results);
                log.debug("results has been created and put into session");
            }
        }
        log.debug("results returned");
        return results;
    }

    public int getStartIndex()
    {
        return startIndex;
    }

    public void setStartIndex(int startIndex)
    {
        this.startIndex = startIndex;
    }

    public static String appendAmpsandOrQuestionMark(String str)
    {
        if (!TextUtils.stringSet(str))
            return str;

        if (str.indexOf("?") != -1)
            return str + "&";
        else
            return str + "?";
    }

    public String getDoSearch()
    {
        return doSearch;
    }

    public void setDoSearch(String doSearch)
    {
        this.doSearch = doSearch;
    }

    public String getSearchQuery()
    {
        return getQuery().getSearchQuery();
    }

    public void setSearchQuery(String searchQuery)
    {
        getQuery().setSearchQuery(searchQuery);
    }

    public String getFormName()
    {
        return getQuery().getFormName();
    }

    public void setFormName(String formName)
    {
        getQuery().setFormName(formName);
    }

    public String getElement()
    {
        return getQuery().getElement();
    }

    public void setElement(String element)
    {
        getQuery().setElement(element);
    }

    public String getConfluenceUrl()
    {
        return getQuery().getConfluenceUrl();
    }

    public void setConfluenceUrl(String confluenceUrl)
    {
        getQuery().setConfluenceUrl(confluenceUrl);
    }

    public void setDoReset(String doReset)
    {
        this.doReset = doReset;
    }

    public String getDoReset()
    {
        return doReset;
    }
}
