package com.atlassian.jira.plugin.linker;

import com.atlassian.jira.issue.customfields.converters.StringConverter;
import com.atlassian.jira.issue.customfields.impl.TextCFType;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;

public class LinkerCFType extends TextCFType
{
    public LinkerCFType(CustomFieldValuePersister customFieldValuePersister, StringConverter stringConverter, GenericConfigManager genericConfigManager)
    {
        super(customFieldValuePersister, stringConverter, genericConfigManager);
    }

    public int compare(Object o1, Object o2)
    {
        return 0;
    }
}
