/*
 * Copyright (c) 2005.
 */
package com.atlassian.jira.plugin.linker;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsManager;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.XmlRpcHandler;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

/**
 */
public class SearchQueryBean implements Serializable
{
    private static Logger log = Logger.getLogger(SearchQueryBean.class);

    private static final String DEFAULT_CONFLUENCE_URL = "http://confluence.atlassian.com";

    private List confluenceUrlList;

    private String formName;
    private String element;

    private String confluenceUrl;
    private String searchQuery;

    private String errorMessage;

    private final User user;

    private final TrustedApplicationsManager trustedApplicationsManager;

    public SearchQueryBean(User user, TrustedApplicationsManager trustedApplicationsManager)
    {
        this.user = user;
        this.trustedApplicationsManager = trustedApplicationsManager;
    }

    private void loadConfluenceUrls()
    {
        Properties props = new Properties(System.getProperties());
        try
        {
            props.load(ClassLoaderUtils.getResourceAsStream("atlassian-jira-linker-plugin.properties", ConfluencePageBrowserAction.class));
            String urlListStr = props.getProperty("confluence.urls");
            if (urlListStr != null)
            {
                confluenceUrlList = Arrays.asList(urlListStr.split("\\s*[,;]\\s*"));
            }
        }
        catch (IOException e)
        {
            log.error(e);
        }

        if (confluenceUrlList == null)
            confluenceUrlList = Collections.singletonList(DEFAULT_CONFLUENCE_URL);
    }

    public List getConfluenceUrlList()
    {
        if (confluenceUrlList == null)
            loadConfluenceUrls();
        return confluenceUrlList;
    }

    public String getSearchQuery()
    {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery)
    {
        this.searchQuery = searchQuery;
    }

    public List doSearch() throws SearchException
    {
        log.debug("doSearch called");
        if (searchQuery != null)
        {
            log.debug("searching for '" + searchQuery + "'");
            try
            {
                errorMessage = null;
                XmlRpcHandler rpcClient = getXmlRpcHandler();
                Object result = rpcClient.execute("confluence1.search", makeParams(getSearchQuery(), 100));
                if (result instanceof Vector)
                {
                    Vector xmlrpcResults = (Vector) result;
                    List searchResults = new ArrayList();
                    for (Iterator iterator = xmlrpcResults.iterator(); iterator.hasNext();)
                    {
                        Hashtable xmlrpcResult = (Hashtable) iterator.next();
                        searchResults.add(new SearchMatch(xmlrpcResult));
                    }
                    return searchResults;
                }
                else if (result instanceof XmlRpcException)
                {
                    throw (XmlRpcException)result;
                }
                else
                {
                    log.error("No result set returned: " + result);
                }
            }
            catch (XmlRpcException e)
            {
                log.error("A problem occurred while searching: " + e.getMessage() + "\n" + (e.getCause() != null ? e.getCause().getMessage() : ""), e);
                errorMessage = (e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
            }
            catch (IOException e)
            {
                log.error("There was an I/O problem", e);
                errorMessage = "Unable to connect to server: " + e.getMessage();                
            }
            catch (Exception e)
            {
                log.error("A problem occurred while searching", e);
                errorMessage = "A serious error occurred while searching: " + e.getMessage();
            }
        }
        log.debug("doSearch returning null");
        return null;
    }

    protected XmlRpcHandler getXmlRpcHandler() throws MalformedURLException {
        URL xmlrpcUrl = new URL(getConfluenceUrl() + "/rpc/xmlrpc");
        return new XmlRpcClient(
                xmlrpcUrl,
                new TrustedApplicationXmlRpcTransportFactory(
                        xmlrpcUrl,
                        user,
                        trustedApplicationsManager
                )
        );
    }

    private Vector makeParams(String query, int maxResults)
    {
        Vector params = new Vector();
        params.add(""); // no authentication token
        params.add(query); // the query itself

        /* if (spaceKey != null)
         {
             Hashtable searchParameters = new Hashtable();
             searchParameters.put("spaceKey", spaceKey);
             params.add(searchParameters);
         }*/

        params.add(new Integer(maxResults)); // maximum # of results

        return params;
    }


    public String getConfluenceUrl()
    {
        if (confluenceUrl == null)
            confluenceUrl = (String)getConfluenceUrlList().get(0);
        return confluenceUrl;
    }

    public void setConfluenceUrl(String confluenceUrl)
    {
        this.confluenceUrl = confluenceUrl;
    }

    public String getFormName()
    {
        return formName;
    }

    public void setFormName(String formName)
    {
        log.debug("setFormName: " + formName);
        this.formName = formName;
    }

    public String getElement()
    {
        log.debug("getElement: " + element);
        return element;
    }

    public void setElement(String element)
    {
        log.debug("setElement: " + element);
        this.element = element;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }
}
