package com.atlassian.jira.plugin.linker;

import org.apache.log4j.Logger;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * <b>Note:</b> Copied verbatim from the bucket.core.action class of the same name.
 *
 * <p>
 * Where there are a _very_ large number of items to paginate (in othe order of 10,000's), loading them all up in memory from the db and then
 * handing them out to the user in a .subList fashion is not very efficient
 *
 * In this scenario we only want to use this bean to compute our next and previous indexes.
 *
 * Hence please use this bean in one of two ways
 * (a) set _items_ on this bean and have the bean slice and dice up the uber collection for you and serve it to you with getPageItems() OR
 * (b) set the total on this bean _only_ and leave the items _alone_. this is recommended for really large collections where its not ideal to load them all up.
 *
 */
public class SearchResultsBean
{
    private static final Logger log = Logger.getLogger(SearchResultsBean.class);

    private List items;
    private int startIndex = 0;
    private int countOnEachPage;
    public static int DEFAULT_COUNT_ON_EACH_PAGE = 10;
    private int total = -1;

    public SearchResultsBean()
    {
        this(DEFAULT_COUNT_ON_EACH_PAGE);
    }

    public SearchResultsBean(int countOnEachPage)
    {
        if (countOnEachPage < 1)
            throw new IllegalArgumentException("Count should be greater than zero!");

        this.countOnEachPage = countOnEachPage;
    }

    public List getItems()
    {
        return items;
    }

    public void setItems(List items)
    {
        this.items = items;
    }

    public void setStartIndex(int startIndex)
    {
        this.startIndex = startIndex;
    }

    public int getEndIndex()
    {
        int endIndex = getStartIndex() + countOnEachPage;

        if (endIndex > getTotal())
            return getTotal();
        else
            return endIndex;
    }

    public int getStartIndex()
    {
        if (startIndex > getTotal())
            return getTotal();
        else if (startIndex < 0)
            return 0;
        else
            return startIndex;
    }

    public int getNextIndex()
    {
        List nextIndexes = getNextIndexes();
        if (nextIndexes != null)
            return ((Integer)nextIndexes.get(0)).intValue();
        return -1;
    }

    public int getPreviousIndex()
    {
        List previousIndexes = getPreviousIndexes();
        if (previousIndexes != null)
            return ((Integer)previousIndexes.get(previousIndexes.size() - 1)).intValue();
        return -1;
    }

    public List getNextIndexes()
    {
        int index = getEndIndex();

        if (index == getTotal())
            return null; //No next page!

        int count = (getTotal() - index) / countOnEachPage;
        if ((getTotal() - index) % countOnEachPage > 0)
            count++;

        List result = new ArrayList(count);

        for (int i = 0; i < count; i++)
        {
            result.add(new Integer(index));
            index += countOnEachPage;
        }
        return result;
    }

    public List getPreviousIndexes()
    {
        int index = getStartIndex();

        if (index == 0)
        {
            log.debug("no previous start indexes.");
            return null; //No previous page!
        }

        int count = index / countOnEachPage;
        if (index % countOnEachPage > 0)
            count++;

        Integer[] result = new Integer[count];

        for (int i = count - 1; i >= 0; i--)
        {
            index -= countOnEachPage;
            result[i] = new Integer(index);
        }

        log.debug("previous start indexes: " + result.length);
        return Arrays.asList(result);
    }

    public int getNiceStartIndex()
    {
        return getStartIndex() + 1;
    }

    /**
     * @return a subset of the list of items passed in, based on startIndex and the max result per page
     */
    public List getPageItems()
    {
        return getItems().subList(getStartIndex(), getEndIndex());
    }

    public int getTotal()
    {
        if (items != null)
            return items.size();
        else
            return total;
    }

    public void setTotal(int total)
    {
        this.total = total;
    }
}
