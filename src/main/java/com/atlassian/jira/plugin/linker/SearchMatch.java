/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

package com.atlassian.jira.plugin.linker;

import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;

import java.util.Hashtable;

public class SearchMatch
{
    private static final Logger log = Logger.getLogger(SearchMatch.class);


    private String title;
    private String url;
    private String excerpt;
    private String type;
    private String icon;

    public SearchMatch()
    {
    }

    public SearchMatch(String title, String url)
    {
        setTitle(title);
        setUrl(url);
    }

    public SearchMatch(Hashtable xmlrpcResult)
    {
        setTitle((String) xmlrpcResult.get("title"));
        setUrl((String) xmlrpcResult.get("url"));

        setExcerpt((String) xmlrpcResult.get("excerpt"));

        String type = (String) xmlrpcResult.get("type");

        if ("page".equals(type))
        {
            setType("Page");
            setIcon("docs_16.gif");
        }
        else if ("blogpost".equals(type))
        {
            setType("News Item");
            setIcon("blogentry_16.gif");
        }
        else if ("comment".equals(type))
        {
            setType("Comment");
            setIcon("comment_16.gif");
        }
        else if ("attachment".equals(type))
        {
            setType("Attachment");
            String lcUrl = url.toLowerCase();
            int qs = lcUrl.indexOf('?');
            if (qs > -1)
                lcUrl = lcUrl.substring(0, qs);
            
            if (lcUrl.endsWith(".xls"))
                setIcon("attachments/excel.gif");
            else if (lcUrl.endsWith(".html") || lcUrl.endsWith(".htm"))
                setIcon("attachments/html.gif");
            else if (lcUrl.endsWith(".gif") || lcUrl.endsWith(".png")
                    || lcUrl.endsWith(".jpg") || lcUrl.endsWith(".bmp")
                    || lcUrl.endsWith(".jpeg"))
                setIcon("attachments/image.gif");
            else if (lcUrl.endsWith(".jar") || lcUrl.endsWith(".class") || lcUrl.endsWith(".java"))
                setIcon("attachments/java.gif");
            else if (lcUrl.endsWith(".pdf"))
                setIcon("attachments/pdf.gif");
            else if (lcUrl.endsWith(".ppt"))
                setIcon("attachments/powerpoint.gif");
            else if (lcUrl.endsWith(".txt"))
                setIcon("attachments/text.gif");
            else if (lcUrl.endsWith(".doc"))
                setIcon("attachments/word.gif");
            else if (lcUrl.endsWith(".xml"))
                setIcon("attachments/xml.gif");
            else if (lcUrl.endsWith(".zip") || lcUrl.endsWith(".gz") || lcUrl.endsWith(".tar"))
                setIcon("attachments/zip.gif");
            else
                setIcon("attachments/file.gif");
        }
        else if ("spacedesc".equals(type))
        {
            setType("Space");
            setIcon("web_16.gif");
        }
        else if ("profile".equals(type) || "userinfo".equals(type))
        {
            setType("User Profile");
            setIcon("user_16.gif");
        }
        else if ("mail".equals(type))
        {
        	setType("Mail");
        	setIcon("mail_16.gif");
        }
        else
        {
            log.warn("Unknown content type: " + type);
            setType("Unknown");
            setIcon("help_16.gif");
        }
    }

    public String getUrl()
    {
        return url;
    }

    public String getTrimmedUrl(int maxLength)
    {
        return StringUtils.abbreviate(url, maxLength);
    }

    public String getTitle()
    {
        return title;
    }

    public String getTrimmedTitle(int maxLength)
    {
        return StringUtils.abbreviate(title, maxLength);
    }

    public String getExcerpt()
    {
        return excerpt;
    }

    public void setExcerpt(String excerpt)
    {
        this.excerpt = excerpt;
    }


    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public String getIcon()
    {
        return icon;
    }
}
