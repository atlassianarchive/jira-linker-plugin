package com.atlassian.jira.plugin.linker;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.security.auth.trustedapps.EncryptedCertificate;
import com.atlassian.security.auth.trustedapps.TrustedApplicationUtils;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsManager;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.DefaultXmlRpcTransport;
import org.apache.xmlrpc.DefaultXmlRpcTransportFactory;
import org.apache.xmlrpc.XmlRpcClientException;
import org.apache.xmlrpc.XmlRpcTransport;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

public class TrustedApplicationXmlRpcTransportFactory extends DefaultXmlRpcTransportFactory
{
    private static final Logger LOG = Logger.getLogger(TrustedApplicationXmlRpcTransportFactory.class);

    private final User user;

    private final TrustedApplicationsManager trustedApplicationsManager;

    public TrustedApplicationXmlRpcTransportFactory(URL url, User user, TrustedApplicationsManager trustedApplicationsManager)
    {
        super(url);
        this.user = user;
        this.trustedApplicationsManager = trustedApplicationsManager;
    }

    public XmlRpcTransport createTransport() throws XmlRpcClientException
    {
        return new TrustedApplicationXmlRpcTransport(url, user, trustedApplicationsManager);
    }

    public void setProperty(String propertyName, Object value)
    {
        throw new UnsupportedOperationException("Everything must be set at construction time. Sorry.");
    }

    private static class TrustedApplicationXmlRpcTransport extends DefaultXmlRpcTransport
    {
        private final User user;

        private final TrustedApplicationsManager trustedApplicationsManager;

        private TrustedApplicationXmlRpcTransport(URL url, User user, TrustedApplicationsManager trustedApplicationsManager)
        {
            super(url);
            this.user = user;
            this.trustedApplicationsManager = trustedApplicationsManager;
        }

        private void setTrustedApplicationHeaders()
        {
            if (null != user)
            {
                EncryptedCertificate encryptedCertificate = trustedApplicationsManager.getCurrentApplication().encode(
                        user.getName()
                );

                con.addRequestProperty(TrustedApplicationUtils.Header.Request.ID, encryptedCertificate.getID());
                con.addRequestProperty(TrustedApplicationUtils.Header.Request.CERTIFICATE, encryptedCertificate.getCertificate());
                con.addRequestProperty(TrustedApplicationUtils.Header.Request.SECRET_KEY, encryptedCertificate.getSecretKey());
            }
        }

        public InputStream sendXmlRpc(byte[] request) throws IOException
        {
            /* Mostly copied from the super class */
            con = url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setUseCaches(false);
            con.setAllowUserInteraction(false);
            con.setRequestProperty("Content-Length",
                    Integer.toString(request.length));
            con.setRequestProperty("Content-Type", "text/xml");

            setTrustedApplicationHeaders();

            OutputStream out = null;

            try
            {
                out = con.getOutputStream();
                
                out.write(request);
                out.flush();

                return con.getInputStream();
            }
            finally
            {
                String trustedAppsErrorMessage = con.getHeaderField(TrustedApplicationUtils.Header.Response.ERROR);
                if (null != trustedAppsErrorMessage)
                    LOG.error("Unable to establish a successful trusted connection: " + trustedAppsErrorMessage);
                
                IOUtils.closeQuietly(out);
            }
        }
    }
}
