package it.com.atlassian.jira.plugin.linker;

import net.sourceforge.jwebunit.TestContext;
import org.xml.sax.SAXException;
import org.apache.commons.lang.StringUtils;
import com.meterware.httpunit.WebLink;


public class TestConfluenceBrowserTrusted extends TestConfluenceBrowser
{
    @Override
    protected void enableConfluenceAnonymousXmlRpc()
    {
        /* Do nothing, because we don't want anonymous access for all the tests */
    }

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        disableConfluenceAnonymousXmlRpc();
        makeConfluenceTrustJira();
    }

    @Override
    public void tearDownTest()
    {
        try
        {
            makeConfluenceFearJira();
        }
        catch (SAXException saxe)
        {
            fail("Failed to remove JIRA trusted application from Confluence.");
        }
        finally
        {
            super.tearDownTest();
        }
    }

    protected void makeConfluenceTrustJira()
    {
        final TestContext testContext = getTester().getTestContext();
        final String jiraBaseUrl = testContext.getBaseUrl();

        navigation.logout();

        testContext.setBaseUrl(getConfluenceBaseUrl());

        /* Send a request to JIRA
         */
        navigation.gotoPage("/admin/trustedapp-view.action?os_username=admin&os_password=admin");
        tester.setWorkingForm("add-trusted-app");
        tester.setFormElement("applicationName", jiraBaseUrl);
        tester.submit("Send Request");

        /* Set trusted app details */
        tester.setWorkingForm("edit-trusted-app");
        tester.setFormElement("ipRestrictions", "127.0.0.1");
        tester. setFormElement("urlRestrictions", "/rpc/xmlrpc");
        tester.submit("save");

        testContext.setBaseUrl(jiraBaseUrl);

        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    protected void makeConfluenceFearJira() throws SAXException
    {
        final TestContext testContext = getTester().getTestContext();
        final String jiraBaseUrl = testContext.getBaseUrl();

        navigation.logout();

        testContext.setBaseUrl(getConfluenceBaseUrl());

        navigation.gotoPage("/admin/trustedapp-view.action?os_username=admin&os_password=admin");
        WebLink[] webLinks = getTester().getDialog().getResponse().getLinks();
        boolean jiraTrustedAppDeleted = false;

        for (WebLink webLink : webLinks)
        {
            if (StringUtils.equalsIgnoreCase("Del", webLink.asText()))
            {
                String urlString = webLink.getURLString();

                navigation.gotoPage("/admin/" + urlString); /* Deletes the trusted app  (JIRA) */
                jiraTrustedAppDeleted = true;
            }
        }

        assertTrue(jiraTrustedAppDeleted);

        testContext.setBaseUrl(jiraBaseUrl);
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }
}
