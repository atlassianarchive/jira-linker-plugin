package it.com.atlassian.jira.plugin.linker;

public class TestConfluenceBrowser extends AbstractLinkerTest {

    public void testIsFieldPresent() {
        navigation.issue().goToCreateIssueForm("Test Project", "Bug");
        tester.assertTextPresent("Test Link Field");
    }

    public void testIsPageHavingRequiredFieldsAndLabels() {
        final String[] confluenceUrls = getConfiguredConfluenceSites();
        navigation.gotoPage(getConfluenceBrowserUrl());

        tester.assertTextPresent("Confluence Site:");
        for (int i = 0; i < confluenceUrls.length; ++i) {
            tester.assertTextPresent(confluenceUrls[i]);
        }

        tester.assertTextPresent("Enter your query below");
        tester.assertFormElementEquals("searchQuery", "");
        tester.assertSubmitButtonPresent("doSearch");
    }

    public void testSearchWhichWillReturnNoResult() {
        navigation.gotoPage(getConfluenceBrowserUrl());

        tester.setFormElement("searchQuery", "spacekey:AREALLYWIERDSPACEKEYTHATSHOULDNOTEXIST AND title:\"JIRA Linker Plugin\"");
        tester.submit();

        tester.assertTextNotPresent("Results");
        tester. assertTextNotPresent("Select this page");
    }

    public void testSearchResultPagination() {
        navigation.gotoPage(getConfluenceBrowserUrl());

        tester.setFormElement("searchQuery", "confluence");
        tester.submit();

        tester.assertTextPresent("Results");
        tester.assertTextPresent("1 - 10");
        tester.assertTextPresent("Select this page");
        tester.assertLinkPresentWithText("Next >>");

        for (int i = 2; i <= 3; ++i) { /* We are sure we have only 3 pages, which otherwise means the search result *is* paginated. */
            tester.assertLinkPresentWithText(String.valueOf(i));
        }
    }

    public void testGoToLastPageOfSearchResult() {
        final StringBuffer stringBuffer = new StringBuffer();

        navigation.gotoPage(getConfluenceBrowserUrl());

        tester.setFormElement("searchQuery", "confluence");
        tester.submit();
        /* The query above will definitely return us 24 results, based on the Confluence data */

        tester.assertTextPresent("Results");
        tester.assertTextPresent("1 - 10");
        tester.assertTextPresent("Select this page");
        tester.assertLinkPresentWithText("Next >>");
        tester.assertLinkNotPresentWithText("<< Previous");


        for (int i = 2; i <= 3; ++i) {
            stringBuffer.setLength(0);


            tester.clickLinkWithText("Next >>");

            tester.assertTextPresent("Results");
            tester.assertTextPresent(stringBuffer.append(((i - 1) * 10) + 1).append(" - ").append(i < 3 ? i * 10: 24).toString());
            tester.assertLinkPresentWithText("<< Previous");

            if (i == 3)
                tester.assertLinkNotPresentWithText("Next >>"); /* Last page, the next link should not be there */
            else
                tester.assertLinkPresentWithText("Next >>");
        }
    }
}
