package it.com.atlassian.jira.plugin.linker;

import com.atlassian.jira.functest.framework.AdministrationImpl;
import com.atlassian.jira.functest.framework.admin.Attachments;
import com.atlassian.jira.functest.framework.admin.AttachmentsImpl;
import com.atlassian.jira.webtests.ztests.imports.project.AbstractProjectImportTestCase;

import java.io.File;

public class TestProjectImportValidation extends AbstractProjectImportTestCase
{
    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        this.administration = new AdministrationImpl(getTester(), getEnvironmentData(), navigation, assertions)
        {
            @Override
            public Attachments attachments()
            {
                return new AttachmentsImpl(getTester(), getEnvironmentData(), navigation)
                {
                    @Override
                    public void enable()
                    {
                        /* The code below is generally the same as that in AttachmentsImpl, except for the removal of the attachmentPathOption radio button checking, which caused problems for the tests in this plugin */
                        log("Enabling attachments with default path.");
                        navigation.gotoAdmin();
                        tester.clickLink("attachments");
                        tester.clickLinkWithText("Edit Settings");
//                        tester.checkCheckbox("attachmentPathOption", "CUSTOM");
                        tester.submit("Update");
                    }
                };
            }
        };
    }

    public void testImportWithValidUrlAsLinkerCfTypeValue()
    {
        File tempFile = null;

        try
        {
            tempFile = doProjectImport("project-import-with-normal-link.zip", "blank-instance-with-only-link-cf.zip", true);
            tester.submit("Import");
            advanceThroughWaitingPage();

            navigation.issue().viewIssue("MKY-1");
            
            tester.assertLinkPresentWithText("http://confluence.atlassian.com/display/DS/Confluence+Overview");
        }
        finally
        {
            if (null != tempFile)
                tempFile.delete();
        }
    }

    public void testImportWithBlankAsLinkerCfTypeValue()
    {
        File tempFile = null;

        try
        {
            tempFile = doProjectImport("project-import-with-blank-link.zip", "blank-instance-with-only-link-cf.zip", true);
            tester.submit("Import");
            advanceThroughWaitingPage();

            navigation.issue().viewIssue("MKY-1");

            /* Blanks should not be imported as valid URL field */
            tester.assertTextNotPresent("Test Link Field");
        }
        finally
        {
            if (null != tempFile)
                tempFile.delete();
        }
    }

    public void testImportWithLoremIpsumAsLinkerCfTypeValue()
    {
        File tempFile = null;

        try
        {
            tempFile = doProjectImport("project-import-with-loremipsum-as-link.zip", "blank-instance-with-only-link-cf.zip", true);
            tester.submit("Import");
            advanceThroughWaitingPage();

            navigation.issue().viewIssue("MKY-1");

            /* Make sure that the whole first line of lorem ipsum becomes the hyperlink */
            tester.assertLinkPresentWithText("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
        }
        finally
        {
            if (null != tempFile)
                tempFile.delete();
        }
    }
}
