package it.com.atlassian.jira.plugin.linker;

public class TestLinkField extends AbstractLinkerTest {

    public void testFieldPresentInIssueView() {
        navigation.issue().gotoIssue("TP-1");
        tester.assertTextPresent("Test Link Field");
        tester.assertTextPresent("http://confluence.atlassian.com/display/JIRAEXT/JIRA+Linker+Plugin");
    }

    public void testEditLink() {
        navigation.issue().gotoIssue("TP-1");
        tester.clickLink("edit-issue");

        tester.assertTextNotPresent("http://confluence.atlassian.com/x/WB4C");

        tester.setFormElement(CUSTOM_FIELD_ID, "http://confluence.atlassian.com/x/WB4C");
        tester.submit();

        tester.assertTextPresent("http://confluence.atlassian.com/x/WB4C");
    }
}
