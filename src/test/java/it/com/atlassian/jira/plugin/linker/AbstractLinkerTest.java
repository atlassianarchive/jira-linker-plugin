package it.com.atlassian.jira.plugin.linker;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.TestContext;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

public abstract class AbstractLinkerTest extends FuncTestCase {

    static final String CUSTOM_FIELD_ID = "customfield_10000";

    Properties pluginConfiguration;

//    public AbstractLinkerTest() {
//        super(AbstractLinkerTest.class.getName());
//    }

    @Override
    protected void setUpTest() {
        super.setUpTest();
        administration.restoreData("testdata-export.zip");
        readPluginConfiguration();
        enableConfluenceAnonymousXmlRpc();
    }

    void readPluginConfiguration() {
        InputStream in = null;

        try {
            
            in = new BufferedInputStream(getClass().getClassLoader().getResourceAsStream("atlassian-jira-linker-plugin.properties"));

            pluginConfiguration = new Properties();
            pluginConfiguration.load(in);

        } catch (final IOException ioe) {
            fail("Unexpected IOException while reading plugin atlassian-jira-linker-plugin.properties: " + ioe.getMessage());
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    String[] getConfiguredConfluenceSites() {
        return StringUtils.split(pluginConfiguration.getProperty("confluence.urls"), " ,");
    }

    String getConfluenceBrowserUrl() {
        return "/secure/popups/ConfluencePageBrowser.jspa?doReset=true&formName=jiraform&element=" + CUSTOM_FIELD_ID;
    }

    String getConfluenceBaseUrl() {
        final JIRAEnvironmentData jiraEnvironmentData = getEnvironmentData();
        final URL baseUrl = jiraEnvironmentData.getBaseUrl();

        return new StringBuffer(baseUrl.getProtocol())
            .append("://")
            .append(baseUrl.getHost())
            .append(":")
            .append(jiraEnvironmentData.getProperty("confluence.http.port"))
            .append("/confluence")
            .toString();
    }

    private void setEnabledAnonymousXmlRpc(boolean enableAnonXmlRpc)
    {
        final TestContext testContext = getTester().getTestContext();
        final String jiraBaseUrl = testContext.getBaseUrl();

        navigation.logout();

        testContext.setBaseUrl(getConfluenceBaseUrl());

        /* Enable anonymous XMLRPC access */
        navigation.gotoPage("/admin/editgeneralconfig.action?" + "os_username=admin&os_password=admin");
        tester.setWorkingForm("editgeneralconfig");

        tester.setFormElement("domainName", getConfluenceBaseUrl());
        tester.checkCheckbox("allowRemoteApiAnonymous", String.valueOf(enableAnonXmlRpc));

        tester.submit("confirm");

        /* Ensure anonymous XMLRPC enabled */
        navigation.gotoPage("/admin/editgeneralconfig.action");
        tester.assertRadioOptionSelected("allowRemoteApiAnonymous", String.valueOf(enableAnonXmlRpc));


        /* Give anonymous user USE permission */
        navigation.gotoPage("/admin/permissions/editglobalpermissions.action");
        tester.setWorkingForm("editglobalperms");
        tester.checkCheckbox("confluence_checkbox_useconfluence_anonymous");
        tester.submit();


        testContext.setBaseUrl(jiraBaseUrl);

        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    void enableConfluenceAnonymousXmlRpc() {
        setEnabledAnonymousXmlRpc(true);
    }

    void disableConfluenceAnonymousXmlRpc() {
        setEnabledAnonymousXmlRpc(false);
    }
}
