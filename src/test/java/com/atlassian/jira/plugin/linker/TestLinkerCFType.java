package com.atlassian.jira.plugin.linker;

import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.converters.StringConverter;
import org.jmock.MockObjectTestCase;

public class TestLinkerCFType extends MockObjectTestCase {

    private CustomFieldValuePersister customFieldValuePersister;

    private StringConverter stringConverter;

    private GenericConfigManager genericConfigManager;

    protected LinkerCFType getLinkerCFType() {
        return new LinkerCFType(customFieldValuePersister, stringConverter, genericConfigManager);
    }

    public void testCompare() {
        LinkerCFType linkerCFType;

        linkerCFType = getLinkerCFType();
        assertEquals(0, linkerCFType.compare(null, null));
        assertEquals(0, linkerCFType.compare(new Object(), null));
        assertEquals(0, linkerCFType.compare(null, new Object()));
        assertEquals(0, linkerCFType.compare(new Object(), new Object()));
    }
}
