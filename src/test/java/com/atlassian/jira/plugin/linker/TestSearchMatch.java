package com.atlassian.jira.plugin.linker;

import junit.framework.TestCase;

import java.util.Map;
import java.util.Hashtable;
import java.util.HashMap;
import java.util.Iterator;

import com.atlassian.core.util.map.EasyMap;
import org.apache.commons.lang.StringUtils;

public class TestSearchMatch extends TestCase {

    private Map pageProperties;

    private Map blogpostProperties;

    private Map commentProperties;

    private Map attachmentProperties;

    private Map spaceDescriptionProperties;

    private Map userinfoProperties;

    private Map profileProperties;
    
    private Map mailProperties;


    protected void setUp() throws Exception {
        Map commonProperties = EasyMap.build(
                "title", "Test Title",
                "url", "http://foo.bar",
                "excerpt", "Test"
        );

        (pageProperties = EasyMap.build("type", "page")).putAll(commonProperties);
        (blogpostProperties = EasyMap.build("type", "blogpost")).putAll(commonProperties);
        (commentProperties = EasyMap.build("type", "comment")).putAll(commonProperties);
        (attachmentProperties = EasyMap.build("type", "attachment")).putAll(commonProperties);
        (spaceDescriptionProperties = EasyMap.build("type", "spacedesc")).putAll(commonProperties);
        (userinfoProperties = EasyMap.build("type", "userinfo")).putAll(commonProperties);
        (profileProperties = EasyMap.build("type", "profile")).putAll(commonProperties);
        (mailProperties = EasyMap.build("type", "mail")).putAll(commonProperties);
    }

    protected SearchMatch getSearchMatch(final Map properties) {
        return new SearchMatch(new Hashtable(properties));
    }

    public void testMatchPage() {
        SearchMatch searchMatch;

        searchMatch = getSearchMatch(pageProperties);

        assertEquals("Test Title", searchMatch.getTitle());
        assertEquals("http://foo.bar", searchMatch.getUrl());
        assertEquals("Test", searchMatch.getExcerpt());

        assertEquals("Page", searchMatch.getType());
        assertEquals("docs_16.gif", searchMatch.getIcon());
    }

    public void testMatchBlogpost() {
        SearchMatch searchMatch;

        searchMatch = getSearchMatch(blogpostProperties);

        assertEquals("Test Title", searchMatch.getTitle());
        assertEquals("http://foo.bar", searchMatch.getUrl());
        assertEquals("Test", searchMatch.getExcerpt());

        assertEquals("News Item", searchMatch.getType());
        assertEquals("blogentry_16.gif", searchMatch.getIcon());
    }

    public void testMatchComment() {
        SearchMatch searchMatch;

        searchMatch = getSearchMatch(commentProperties);

        assertEquals("Test Title", searchMatch.getTitle());
        assertEquals("http://foo.bar", searchMatch.getUrl());
        assertEquals("Test", searchMatch.getExcerpt());

        assertEquals("Comment", searchMatch.getType());
        assertEquals("comment_16.gif", searchMatch.getIcon());
    }

    public void testMatchAttachment() {

        Map urlsAndExpectedIconPaths = new HashMap();

        urlsAndExpectedIconPaths.put("http://foo.bar/test.xls", "attachments/excel.gif");

        urlsAndExpectedIconPaths.put("http://foo.bar/test.html", "attachments/html.gif");
        urlsAndExpectedIconPaths.put("http://foo.bar/test.htm", "attachments/html.gif");

        urlsAndExpectedIconPaths.put("http://foo.bar/test.gif", "attachments/image.gif");
        urlsAndExpectedIconPaths.put("http://foo.bar/test.png", "attachments/image.gif");
        urlsAndExpectedIconPaths.put("http://foo.bar/test.jpg?foo=bar&bar=foo", "attachments/image.gif");
        urlsAndExpectedIconPaths.put("http://foo.bar/test.jpeg?foo=bar&bar=foo", "attachments/image.gif");
        urlsAndExpectedIconPaths.put("http://foo.bar/test.bmp", "attachments/image.gif");

        urlsAndExpectedIconPaths.put("http://foo.bar/test.jar", "attachments/java.gif");
        urlsAndExpectedIconPaths.put("http://foo.bar/test.java", "attachments/java.gif");
        urlsAndExpectedIconPaths.put("http://foo.bar/test.class", "attachments/java.gif");

        urlsAndExpectedIconPaths.put("http://foo.bar/test.pdf", "attachments/pdf.gif");

        urlsAndExpectedIconPaths.put("http://foo.bar/test.ppt", "attachments/powerpoint.gif");

        urlsAndExpectedIconPaths.put("http://foo.bar/test.txt", "attachments/text.gif");

        urlsAndExpectedIconPaths.put("http://foo.bar/test.doc", "attachments/word.gif");

        urlsAndExpectedIconPaths.put("http://foo.bar/test.xml", "attachments/xml.gif");

        urlsAndExpectedIconPaths.put("http://foo.bar/test.zip", "attachments/zip.gif");

        urlsAndExpectedIconPaths.put("http://foo.bar/test.exe", "attachments/file.gif");
        urlsAndExpectedIconPaths.put("http://foo.bar/test.bat", "attachments/file.gif");
        urlsAndExpectedIconPaths.put("http://foo.bar/test.sh", "attachments/file.gif");
        

        for (final Iterator i = urlsAndExpectedIconPaths.entrySet().iterator(); i.hasNext();) {
            final Map.Entry e = (Map.Entry) i.next();
            final Map _attachmentProperties = new HashMap(attachmentProperties);
            final SearchMatch searchMatch;

            _attachmentProperties.put("url", e.getKey());
            searchMatch = getSearchMatch(_attachmentProperties);

            assertEquals("Test Title", searchMatch.getTitle());
            assertEquals("Test", searchMatch.getExcerpt());

            assertEquals(e.getValue(), searchMatch.getIcon());
        }
    }

    public void testMatchSpaceDescription() {
        SearchMatch searchMatch;

        searchMatch = getSearchMatch(spaceDescriptionProperties);

        assertEquals("Test Title", searchMatch.getTitle());
        assertEquals("http://foo.bar", searchMatch.getUrl());
        assertEquals("Test", searchMatch.getExcerpt());

        assertEquals("Space", searchMatch.getType());
        assertEquals("web_16.gif", searchMatch.getIcon());
    }

    public void testMatchUserInfoOrProfile() {
        SearchMatch searchMatch;

        searchMatch = getSearchMatch(userinfoProperties);

        assertEquals("Test Title", searchMatch.getTitle());
        assertEquals("http://foo.bar", searchMatch.getUrl());
        assertEquals("Test", searchMatch.getExcerpt());

        assertEquals("User Profile", searchMatch.getType());
        assertEquals("user_16.gif", searchMatch.getIcon());

        searchMatch = getSearchMatch(profileProperties);

        assertEquals("Test Title", searchMatch.getTitle());
        assertEquals("http://foo.bar", searchMatch.getUrl());
        assertEquals("Test", searchMatch.getExcerpt());

        assertEquals("User Profile", searchMatch.getType());
        assertEquals("user_16.gif", searchMatch.getIcon());
    }

    public void testMatchMail() {
        SearchMatch searchMatch;

        searchMatch = getSearchMatch(mailProperties);

        assertEquals("Test Title", searchMatch.getTitle());
        assertEquals("http://foo.bar", searchMatch.getUrl());
        assertEquals("Test", searchMatch.getExcerpt());

        assertEquals("Mail", searchMatch.getType());
        assertEquals("mail_16.gif", searchMatch.getIcon());
    }

    public void testMatchUnknownContent() {
        SearchMatch searchMatch;
        Map properties;

        properties = new HashMap(pageProperties);
        properties.put("type", "foo");
        searchMatch = getSearchMatch(properties);

        assertEquals("Test Title", searchMatch.getTitle());
        assertEquals("http://foo.bar", searchMatch.getUrl());
        assertEquals("Test", searchMatch.getExcerpt());
        assertEquals("Unknown", searchMatch.getType());
        assertEquals("help_16.gif", searchMatch.getIcon());

        properties = new HashMap(pageProperties);
        properties.put("type", "bar");
        searchMatch = getSearchMatch(properties);

        assertEquals("Test Title", searchMatch.getTitle());
        assertEquals("http://foo.bar", searchMatch.getUrl());
        assertEquals("Test", searchMatch.getExcerpt());
        assertEquals("Unknown", searchMatch.getType());
        assertEquals("help_16.gif", searchMatch.getIcon());

        properties = new HashMap(pageProperties);
        properties.put("type", StringUtils.EMPTY);
        searchMatch = getSearchMatch(properties);

        assertEquals("Test Title", searchMatch.getTitle());
        assertEquals("http://foo.bar", searchMatch.getUrl());
        assertEquals("Test", searchMatch.getExcerpt());
        assertEquals("Unknown", searchMatch.getType());
        assertEquals("help_16.gif", searchMatch.getIcon());
    }

    public void testTrimmedProperties() {
        String title;
        String url;
        SearchMatch sm;

        title = "Test Title";
        url = "Test URL";
        sm = new SearchMatch(title, url);

        assertEquals("Test...", sm.getTrimmedTitle(7));
        assertEquals("Test...", sm.getTrimmedUrl(7));
        assertEquals("Test Title", sm.getTrimmedTitle(Integer.MAX_VALUE));
        assertEquals("Test URL", sm.getTrimmedUrl(Integer.MAX_VALUE));
        assertEquals("Test Title", sm.getTitle());
        assertEquals("Test URL", sm.getUrl());

    }
}
