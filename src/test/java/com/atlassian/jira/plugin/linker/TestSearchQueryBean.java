package com.atlassian.jira.plugin.linker;

import com.atlassian.jira.util.EasyList;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.XmlRpcHandler;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;
import org.jmock.core.Stub;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

public class TestSearchQueryBean extends MockObjectTestCase {

    private XmlRpcHandler xmlRpcHandler;

    protected SearchQueryBean getSearchQueryBean() {
        return new SearchQueryBean(null, null) {

            protected XmlRpcHandler getXmlRpcHandler() throws MalformedURLException {
                return xmlRpcHandler;
            }
        };
    }

    public void testGetConfluenceUrlList() {
        SearchQueryBean searchQueryBean;
        List urls;

        searchQueryBean = getSearchQueryBean();
        urls = searchQueryBean.getConfluenceUrlList();

        assertEquals(1, urls.size());
        assertEquals("http://confluence.atlassian.com", (String) urls.get(0));
        assertEquals("http://confluence.atlassian.com", searchQueryBean.getConfluenceUrl());

        searchQueryBean.setConfluenceUrl("http://foo.bar");
        assertEquals("http://foo.bar", searchQueryBean.getConfluenceUrl());
    }

    public void testDoSearch() throws SearchException {
        Mock mockXmlRpcHandler;
        String searchQuery;
        SearchQueryBean searchQueryBean;

        searchQuery = "foobar";

        mockXmlRpcHandler = new Mock(XmlRpcHandler.class);
        mockXmlRpcHandler
                .expects(exactly(7))
                .method("execute")
                .with(eq("confluence1.search"), isA(Vector.class))
                .will(
                        onConsecutiveCalls(
                                new Stub[] {
                                        throwException(new XmlRpcException(0, "Fake XmlRpcException")),
                                        throwException(new XmlRpcException(0, "Fake XmlRpcException", new RuntimeException("Fake RuntimeException"))),
                                        throwException(new IOException("Fake IOException")),
                                        throwException(new Exception("Fake Exception")),
                                        returnValue(new XmlRpcException(0, "Fake XmlRpcException")),
                                        returnValue(null),
                                        returnValue(
                                                new Vector(
                                                        EasyList.build(
                                                                new Hashtable(
                                                                        new HashMap()
                                                                        {
                                                                            {
                                                                                put("title", "Foo Page Title");
                                                                                put("url", "http://foo.bar");
                                                                                put("excerpt", "Foo");
                                                                                put("type", "page");
                                                                            }
                                                                        }
                                                                )
                                                        )
                                                )
                                        )
                                }
                        )
                );

        xmlRpcHandler = (XmlRpcHandler) mockXmlRpcHandler.proxy();
        searchQueryBean = getSearchQueryBean();

        assertNull(searchQueryBean.doSearch());
        assertNull(searchQueryBean.getErrorMessage());

        searchQueryBean.setSearchQuery(searchQuery);
        assertEquals("foobar", searchQueryBean.getSearchQuery());
        
        assertNull(searchQueryBean.doSearch());
        assertEquals("Fake XmlRpcException", searchQueryBean.getErrorMessage());

        assertNull(searchQueryBean.doSearch());
        assertEquals("Fake RuntimeException", searchQueryBean.getErrorMessage());

        assertNull(searchQueryBean.doSearch());
        assertEquals("Unable to connect to server: Fake IOException", searchQueryBean.getErrorMessage());

        assertNull(searchQueryBean.doSearch());
        assertEquals("A serious error occurred while searching: Fake Exception", searchQueryBean.getErrorMessage());

        assertNull(searchQueryBean.doSearch());
        assertEquals("Fake XmlRpcException", searchQueryBean.getErrorMessage());

        assertNull(searchQueryBean.doSearch());
        assertNull(searchQueryBean.getErrorMessage());

        List result = searchQueryBean.doSearch();
        assertEquals(1, result.size());

        SearchMatch searchMatch = (SearchMatch) result.get(0);

        assertEquals("Foo Page Title", searchMatch.getTitle());
        assertEquals("http://foo.bar", searchMatch.getUrl());
        assertEquals("Foo", searchMatch.getExcerpt());
        assertEquals("Page", searchMatch.getType());
    }

    public void testElementAccessor() {
        SearchQueryBean searchQueryBean;

        searchQueryBean = getSearchQueryBean();

        assertNull(searchQueryBean.getElement());
        searchQueryBean.setElement("foo");
        assertEquals("foo", searchQueryBean.getElement());
    }

    public void testFormAccessor() {
        SearchQueryBean searchQueryBean;

        searchQueryBean = getSearchQueryBean();

        assertNull(searchQueryBean.getFormName());
        searchQueryBean.setFormName("foo");
        assertEquals("foo", searchQueryBean.getFormName());
    }
}
