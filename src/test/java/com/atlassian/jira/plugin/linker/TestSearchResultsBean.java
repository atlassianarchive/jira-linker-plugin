package com.atlassian.jira.plugin.linker;

import junit.framework.TestCase;

import java.util.List;
import java.util.ArrayList;

public class TestSearchResultsBean extends TestCase {

    private List items;


    protected void setUp() throws Exception {
        super.setUp();

        items = new ArrayList();
        for (int i = 0; i < 15; ++i) {
            items.add(new Integer(i));
        }
    }

    protected SearchResultsBean getSearchResultsBean() {
        return new SearchResultsBean();
    }

    public void testEverything() {
        SearchResultsBean searchResultsBean = getSearchResultsBean();

        searchResultsBean.setItems(items);
        assertSame(items, searchResultsBean.getItems());

        assertEquals(items.subList(0, 10), searchResultsBean.getPageItems());
        assertEquals(1, searchResultsBean.getNiceStartIndex());
        assertEquals(0, searchResultsBean.getStartIndex());
        assertEquals(10, searchResultsBean.getEndIndex());
        assertEquals(10, searchResultsBean.getNextIndex());
        assertEquals(-1, searchResultsBean.getPreviousIndex());

        /* Scroll to next page */
        searchResultsBean.setStartIndex(searchResultsBean.getNextIndex());

        assertEquals(items.subList(10, 15), searchResultsBean.getPageItems());
        assertEquals(11, searchResultsBean.getNiceStartIndex());
        assertEquals(10, searchResultsBean.getStartIndex());
        assertEquals(15, searchResultsBean.getEndIndex());
        assertEquals(-1, searchResultsBean.getNextIndex());
        assertEquals(0, searchResultsBean.getPreviousIndex());

        /* Scroll tp previous page */
        searchResultsBean.setStartIndex(searchResultsBean.getPreviousIndex());

        assertEquals(items.subList(0, 10), searchResultsBean.getPageItems());
        assertEquals(1, searchResultsBean.getNiceStartIndex());
        assertEquals(0, searchResultsBean.getStartIndex());
        assertEquals(10, searchResultsBean.getEndIndex());
        assertEquals(10, searchResultsBean.getNextIndex());
        assertEquals(-1, searchResultsBean.getPreviousIndex());
    }
}
